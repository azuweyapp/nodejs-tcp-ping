/**
 * TCP Ping data
 *
 * @export
 * @interface IOptions
 * @property {number | null} ping
 * @property {string} [error]
 */
export interface IPingData {
  ping: number | null;
  error?: string;
}