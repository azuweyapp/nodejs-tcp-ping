/**
 * TCP Ping options
 *
 * @export
 * @interface IOptions
 * @property {number} attempts
 * @property {string} host
 * @property {number} port
 * @property {number} timeout
 */
export interface IOptions {
  attempts: number;
  host: string;
  port: number;
  timeout: number;
}