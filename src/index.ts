import { Socket } from 'net';

import { IOptions, IPingData } from './interfaces';
import { CountDownCallback } from './types';

/**
 * TCP ping function, which return all the ping data from the server.
 *
 * @export
 * @param {{ attempts?: number, host?: string, port?: number, timeout?: number }} [options]
 * @returns {Promise<IPingData[]>}
 */
export function tcpPing(options?: {
  attempts?: number,
  host?: string,
  port?: number,
  timeout?: number
}): Promise<IPingData[]> {
  let _options: IOptions = {
    attempts: options && options.attempts ? options.attempts : 5,
    host: options && options.host ? options.host : 'localhost',
    port: options && options.port ? options.port : 80,
    timeout: options && options.timeout ? options.timeout : 5000
  };

  return new Promise<IPingData[]>((resolve, reject) => {
    let results: IPingData[] = [];

    countDown(_options.host, _options.port, _options.timeout, _options.attempts, (
      error: any,
      done: boolean,
      ellapsedTime?: number
    ) => {
      if (error !== null && error.code !== 'ECONNREFUSED')
        reject(error);
      else if (error !== null && error.code === 'ECONNREFUSED')
        results.push({ ping: null, error: 'Connection timed out' });
      else if (error === null && ellapsedTime)
        results.push({ ping: ellapsedTime });
      if (done)
        resolve(results);
    });
  });
}

/**
 * Count down the remaining attempts and depend on this recursively call himself.
 *
 * @param {string} host
 * @param {number} port
 * @param {number} timeout
 * @param {number} remainingCallsCount
 * @param {CountDownCallback} callback
 */
function countDown(
  host: string,
  port: number,
  timeout: number,
  remainingCallsCount: number,
  callback: CountDownCallback
) {
  --remainingCallsCount;
  connect(host, port, timeout, (error: Error | null, ping?: number) => {
    callback(error, remainingCallsCount === 0, ping);
    if (remainingCallsCount !== 0)
      countDown(host, port, timeout, remainingCallsCount, callback);
  });
}

/**
 * Try connect to the TCP Server, and have tree state: failed, connected or timeout
 *
 * @param {string} host
 * @param {number} port
 * @param {number} timeout
 * @param {((error: Error | null, ping?: number) => void)} callback
 */
function connect(
  host: string,
  port: number,
  timeout: number,
  callback: (error: Error | null, ping?: number) => void
) {
  let socket = new Socket();
  const start = process.hrtime();

  // Timeout state
  socket.setTimeout(timeout, () => {
    socket.destroy();
    callback(null, timeout + 1);
  });

  // Connected state
  socket.connect(port, host, () => {
    const ellapsedTime = process.hrtime(start);
    socket.destroy();
    // Callback with the ellapsed time in millisecond
    callback(null, ((ellapsedTime[0] * 1e9 + ellapsedTime[1]) / 1e6));
  });

  // Failed state
  socket.on('error', (error) => {
    socket.destroy();
    callback(error);
  });
}

export { IPingData } from './interfaces';