/**
 * Count down callback
 *
 * @export
 * @type CountDownCallback
 * @property {Error | null} error
 * @property {boolean} done
 * @property {number} [ellapsedTime]
 */
export type CountDownCallback = (
  error: Error | null,
  done: boolean,
  ellapsedTime?: number
) => void;
