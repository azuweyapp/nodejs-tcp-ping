'use strict';
const chai = require('chai');
const chaiSubset = require('chai-subset');

const pingUtility = require('../lib');
const net = require('net');

const testPort = 8443;
const attempts = 5;
const timeout = 1000;

chai.use(chaiSubset);

describe('Ping functional test', () => {
  const server = net.createServer();

  before((done) => {
    server.listen(testPort, () => done());
  });

  after((done) => {
    server.close(() => done());
  });

  it('Should return a five long array', (done) => {
    pingUtility.tcpPing({
      port: testPort,
      attempts,
      timeout
    }).then(results => {
      chai.expect(results).to.have.lengthOf(attempts);
      done();
    }).catch(reason => {
      done(reason);
    });
  });

  it('Should return a five long array and all of them are should be successful', (done) => {
    pingUtility.tcpPing({
      port: testPort,
      attempts,
      timeout
    }).then(results => {
      chai.expect(results).to.have.lengthOf(attempts)
        .and.to.not.containSubset([{
          ping: null,
          error: 'Connection timed out',
        }]);
      done();
    }).catch(reason => {
      done(reason);
    });
  });

  it('Should return a one long array and should be failed with timed out error', (done) => {
    pingUtility.tcpPing({
      port: testPort + 1,
      attempts: 1,
      timeout
    }).then(results => {
      chai.expect(results).to.have.lengthOf(1)
        .and.to.containSubset([{
          ping: null,
          error: 'Connection timed out',
        }]);
      done();
    }).catch(reason => {
      done(reason);
    });
  });
});